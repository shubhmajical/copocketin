/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.pocketin.GCM;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.quantumgraph.sdk.GcmNotificationIntentService;
import com.quantumgraph.sdk.QG;

import org.json.JSONObject;

import co.pocketin.AdminPanel.SendDeals;
import co.pocketin.R;
import co.pocketin.RealTimeDeal;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        if (data.containsKey("message") && QG.isQGMessage(data.getString("message"))) {
            Intent intent = new Intent(getApplicationContext(), GcmNotificationIntentService.class);
            intent.setAction("QG");
            intent.putExtras(data);
            getApplicationContext().startService(intent);
            return;
        }

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */
        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor=pref.edit();

        try {
            editor.putString("DealTimeID", new JSONObject((message)).getString("deal_time_id"));
            editor.commit();
        }catch(Exception e){
            e.printStackTrace();
        }
        if(RealTimeDeal.realtimedealActive) {
            Intent gcm_rec = new Intent("my_intent");
            gcm_rec.putExtra("deals",message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(gcm_rec);
        }
        else{
            sendNotification(message);
        }
        // [END_EXCLUDE]


    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) {

        Intent intent = new Intent(this, SendDeals.class);
        intent.putExtra("Restaurant Selected", message);
        Log.d("Message to Send deal", message + "");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.pocketin_logo)
                .setContentTitle("Pocketin")
                .setContentText("You have received one new deal!")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
