package co.pocketin;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class BuffetCardAdapter extends RecyclerView.Adapter<BuffetCardAdapter.CardViewHolder> {

    public static List<BuffetCardInfo> list;

    public BuffetCardAdapter(List<BuffetCardInfo> list){
        this.list=list;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(co.pocketin.R.layout.cardview_buffet, viewGroup, false);

        return new CardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {

        BuffetCardInfo ci = list.get(position);
        holder.cost_per_head.setText(ci.cost_per_head);
        holder.cuisines.setText(ci.cuisines);
        holder.restaurant_location.setText(ci.restaurant_location);
        holder.restaurant_image.setImageResource(ci.restaurant_image_id);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {

        protected TextView cost_per_head;
        protected TextView cuisines;
        protected TextView restaurant_location;
        protected ImageView restaurant_image;

        public static List<BuffetCardInfo> select_list= new ArrayList<>();


        public CardViewHolder(View itemView) {
            super(itemView);
            cost_per_head= (TextView) itemView.findViewById(co.pocketin.R.id.costPerHead);
            cuisines= (TextView) itemView.findViewById(co.pocketin.R.id.cuisines);
            restaurant_location= (TextView) itemView.findViewById(co.pocketin.R.id.restrauant_location);
            restaurant_image= (ImageView) itemView.findViewById(co.pocketin.R.id.restaurant_image);

        }
    }

}
