package co.pocketin;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class GroupBookings extends AppCompatActivity implements View.OnClickListener {

    private Button request_call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.pocketin.R.layout.activity_group_bookings);
        Toolbar toolbar = (Toolbar) findViewById(co.pocketin.R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        request_call= (Button) findViewById(co.pocketin.R.id.request_call);
        request_call.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()== co.pocketin.R.id.request_call){
            new PhoneCallMethod().call(this);
        }
    }
}
