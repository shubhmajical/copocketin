package co.pocketin.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import co.pocketin.EventCardAdapter;
import co.pocketin.EventCardInfo;
import co.pocketin.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class EventFragment extends android.support.v4.app.Fragment implements View.OnClickListener {

    View view_fragment;
    RecyclerView recList;
    LinearLayoutManager llm;
    EventCardAdapter card_adapter;
    List<EventCardInfo> list;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * //@param param1 Parameter 1.
     *  // @param param2 Parameter 2.
     * @return A new instance of fragment BuffetFragment.
     */
    public static EventFragment newInstance() {//(String param1, String param2) {
        EventFragment fragment = new EventFragment();
        return fragment;
    }

    public EventFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view_fragment=inflater.inflate(R.layout.fragment_event, container, false);

        /*
        recList = (RecyclerView) view_fragment.findViewById(R.id.eventCardList);
        list=new ArrayList<EventCardInfo>();

        // TODO: Parameter has to fetch from server by passing url.
        // list= new SenderReceiver("URL").receiveFromServer();

        EventCardInfo cd=new EventCardInfo("DJ Natalie","Raasta","8 pm", R.drawable.default_restaurant);
        list.add(cd);
        list.add(cd);
        list.add(cd);
        list.add(cd);
        list.add(cd);
        list.add(cd);
        list.add(cd);

        card_adapter = new EventCardAdapter(list);
        recList.setAdapter(card_adapter);
        recList.setHasFixedSize(true);
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        */

        return view_fragment;

    }

    @Override
    public void onClick(View v) {
    }
}
