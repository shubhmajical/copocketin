package co.pocketin.Fragment;


import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import co.pocketin.AlaCarteCardAdapter;
import co.pocketin.AlaCarteCardInfo;
import co.pocketin.RealTimeDeal;
import co.pocketin.SenderReceiver;
import co.pocketin.Utilities.Constants;

import co.pocketin.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AlaCarteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AlaCarteFragment extends android.support.v4.app.Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match

    View view_fragment;

    RecyclerView recList;
    LinearLayoutManager llm;
    public AlaCarteCardAdapter card_adapter;
    public static List<AlaCarteCardInfo> list;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private static final String TAG = "AlaCarte Fragment";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private SharedPreferences pref;
    public static boolean isPocketinActive = true;

    Button guestButton, timeButton, dateButton, setButton, realTimeDeal;
    public String femaleGuestNumber = "1", maleGuestNumber = "1", visitDate, visitTime;
    int day, month, year, hour, min, day_tomorrow, month_tomorrow, year_tomorrow;
    int np_male, np_female, np_day, np_month, np_hour, np_min;

    EditText et_search_bar;
    ImageButton ib_search_img;
    LinearLayout datetimeChooser;
    private Calendar calendar;

    NumberPicker np1, np2, np3, np4, np5, np6;

    String[] month_number, month_names;
    int[] dates;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * <p/>
     * //@param param1 Parameter 1.
     * // @param param2 Parameter 2.
     *
     * @return A new instance of fragment RecentFragment.
     */
    @SuppressLint("ValidFragment")
    public static AlaCarteFragment newInstance(List<AlaCarteCardInfo> list) {
        AlaCarteFragment fragment = new AlaCarteFragment(list);
        return fragment;
    }

    public AlaCarteFragment(List<AlaCarteCardInfo> list) {
        this.list = list;
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Setting the card list of restaurants to false so that returning to AlaCarteFragment doesnt show perevious selections
        // Inflate the layout for this fragment
        view_fragment = inflater.inflate(R.layout.fragment_alacarte, container, false);
        recList = (RecyclerView) view_fragment.findViewById(R.id.cardList);

        pref = getActivity().getSharedPreferences("MyPref", 0);
        datetimeChooser = (LinearLayout) view_fragment.findViewById(R.id.datetime_spinner);

        guestButton = (Button) view_fragment.findViewById(R.id.guestButton);
        guestButton.setOnClickListener(this);
        dateButton = (Button) view_fragment.findViewById(R.id.dateButton);
        dateButton.setOnClickListener(this);
        timeButton = (Button) view_fragment.findViewById(R.id.timeButton);
        timeButton.setOnClickListener(this);

        setButton = (Button) view_fragment.findViewById(R.id.setButton);
        setButton.setOnClickListener(this);
        realTimeDeal = (Button) view_fragment.findViewById(R.id.real_time_deal);
        realTimeDeal.setOnClickListener(this);

        month_number = getResources().getStringArray(R.array.months);
        month_names = getResources().getStringArray(R.array.months_name);

        calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        min = calendar.get(Calendar.MINUTE);
        if (hour < 11 || hour > 22) {
            Toast toast=Toast.makeText(getContext(), "Pocketin is active from 11:30 am to 11 pm", Toast.LENGTH_LONG);
            ViewGroup group = (ViewGroup) toast.getView();
            TextView messageTextView = (TextView) group.getChildAt(0);
            messageTextView.setTextSize(14);
            toast.show();
            isPocketinActive = false;
            hour = 12;
            min = -15;
        } else if (hour == 11 && min < 30) {
            Toast toast=Toast.makeText(getContext(), "Pocketin is active from 11:30 am to 11 pm", Toast.LENGTH_LONG);
            ViewGroup group = (ViewGroup) toast.getView();
            TextView messageTextView = (TextView) group.getChildAt(0);
            messageTextView.setTextSize(14);
            toast.show();
            isPocketinActive = false;
            hour = 12;
            min = -15;
        } else {
            if (hour == 11 || hour == 12)
                hour -= 0;
            else
                hour -= 12;
        }
        showTime(hour, min + 30);
        day = calendar.get(Calendar.DATE);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);
        showDate(day, month);

        et_search_bar = (EditText) view_fragment.findViewById(R.id.et_search_bar);
        //ib_search_img = (ImageButton) view_fragment.findViewById(R.id.ib_search_img);
        et_search_bar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                List<AlaCarteCardInfo> updatedata = new ArrayList<>();
                AlaCarteCardInfo obj;
                try {
                    for (int i = 0; i < list.size(); i++) {
                        obj = list.get(i);
                        String name = obj.restaurant_name;
                        if (name.toLowerCase().contains(s.toString().toLowerCase())) {
                            updatedata.add(obj);
                        }
                    }
                    if (updatedata.size() == 0) {
                        Toast.makeText(getContext(), "No Restaurants Found", Toast.LENGTH_SHORT).show();
                    }
                    card_adapter.setList(updatedata);
                    card_adapter.notifyDataSetChanged();
                    Log.d("listsize--", "actual-" + list.size() + "searched-" + updatedata.size());
                } catch (Exception e) {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        np1 = (NumberPicker) view_fragment.findViewById(R.id.numberPicker1);
        np2 = (NumberPicker) view_fragment.findViewById(R.id.numberPicker2);
        np3 = (NumberPicker) view_fragment.findViewById(R.id.numberPicker3);
        np4 = (NumberPicker) view_fragment.findViewById(R.id.numberPicker4);
        np5 = (NumberPicker) view_fragment.findViewById(R.id.numberPicker5);
        np6 = (NumberPicker) view_fragment.findViewById(R.id.numberPicker6);

        calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        day_tomorrow = calendar.get(Calendar.DATE);
        month_tomorrow = calendar.get(Calendar.MONTH);
        year_tomorrow = calendar.get(Calendar.YEAR);
        dates = new int[]{day, day_tomorrow};

        setNumberPickerValues();

        card_adapter = new AlaCarteCardAdapter(list, getContext());
        recList.setAdapter(card_adapter);
        recList.setHasFixedSize(true);
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        return view_fragment;
    }

    public void setNumberPickerValues() {
        np1.setMinValue(0);
        np1.setMaxValue(10);
        np1.setValue(1);
        np1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (newVal == 0) {
                    np2.setMinValue(1);
                    np2.setMaxValue(10);
                } else {
                    np2.setMinValue(0);
                    np2.setMaxValue(10);
                }
            }
        });

        np2.setMinValue(0);
        np2.setMaxValue(10);
        np2.setValue(1);
        np2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (newVal == 0) {
                    np1.setMinValue(1);
                    np1.setMaxValue(10);
                } else {
                    np1.setMinValue(0);
                    np1.setMaxValue(10);
                }
            }
        });

        np3.setMinValue(0);
        np3.setMaxValue(1);
        np3.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return dates[value] + "";
            }
        });
        np3.setValue(0);
        np3.setWrapSelectorWheel(false);
        np3.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (newVal == 1) {
                    np5.setMinValue(1);
                    np5.setMaxValue(12);
                    np6.setMinValue(0);
                    np6.setMaxValue(3);
                    np6.setFormatter(new NumberPicker.Formatter() {
                        @Override
                        public String format(int value) {
                            return String.format("%02d", value * 15);
                        }
                    });
                } else {
                    minuteNumberPickerAdjuster();
                }
            }
        });

        if (month == month_tomorrow) {
            np4.setMinValue(0);
            np4.setMaxValue(0);
            np4.setValue(0);
            np4.setFormatter(new NumberPicker.Formatter() {
                @Override
                public String format(int value) {
                    return month_number[month];
                }
            });
        } else {
            np4.setMinValue(0);
            np4.setMaxValue(1);
            np4.setValue(0);
            np4.setFormatter(new NumberPicker.Formatter() {
                @Override
                public String format(int value) {
                    return month_number[value + month];
                }
            });
        }
        if (hour == 11) {
            if (min >= 30) {
                hour = -1;
            }
        }
        if (hour == 12) {
            //To set numberpicker values in the inactive hours of Pocketin.
            if (min == -30) {
                min = 30;
                hour = -1;
            } else {
                //Case which handle when the actual time is around noon
                if (min > 15) {
                    hour = 0;
                }
                else {
                    hour = -1;
                }
            }
        }
        minuteNumberPickerAdjuster();
        np5.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if(newVal == 12 && dates[np3.getValue()] == day){
                    minuteNumberPickerAdjuster();
                }
                else if (newVal == np5.getMinValue() && dates[np3.getValue()] == day && np5.getMaxValue()!=12) {
                    minuteNumberPickerAdjuster();
                } else {
                    np6.setMinValue(0);
                    np6.setMaxValue(3);
                    np6.setFormatter(new NumberPicker.Formatter() {
                        @Override
                        public String format(int value) {
                            return String.format("%02d", value * 15);
                        }
                    });
                }
                if (newVal == 11) {
                    np6.setMinValue(0);
                    np6.setMaxValue(0);
                    np6.setFormatter(new NumberPicker.Formatter() {
                        @Override
                        public String format(int value) {
                            return String.format("%02d", value * 15);
                        }
                    });
                }
            }
        });
    }


    public void minuteNumberPickerAdjuster() {
        if (min >= 15) {
            if (hour == -1) {
                np5.setMinValue(1);
                np5.setMaxValue(12);
                np5.setValue(12);
            } else {
                np5.setMinValue(hour + 1);
                np5.setMaxValue(11);
                np5.setValue(hour + 1);
            }
            if (min <= 30) {
                np6.setMinValue(0);
                np6.setMaxValue(3);
                np6.setFormatter(new NumberPicker.Formatter() {
                    @Override
                    public String format(int value) {
                        return String.format("%02d", value * 15);
                    }
                });
            } else if (min <= 45) {
                np6.setMinValue(1);
                np6.setMaxValue(3);
                np6.setFormatter(new NumberPicker.Formatter() {
                    @Override
                    public String format(int value) {
                        return String.format("%02d", value * 15);
                    }
                });
            } else {
                np6.setMinValue(2);
                np6.setMaxValue(3);
                np6.setFormatter(new NumberPicker.Formatter() {
                    @Override
                    public String format(int value) {
                        return String.format("%02d", value * 15);
                    }
                });
            }
        } else {
            if (hour == -1) {
                np5.setMinValue(1);
                np5.setMaxValue(12);
                np5.setValue(12);
            } else {
                np5.setMinValue(hour);
                np5.setMaxValue(11);
                np5.setValue(hour);
            }
            np6.setMinValue(3);
            np6.setMaxValue(3);
            np6.setFormatter(new NumberPicker.Formatter() {
                @Override
                public String format(int value) {
                    return String.format("%02d", value * 15);
                }
            });
        }
        if (np5.getValue() == 11) {
            np6.setMinValue(0);
            np6.setMaxValue(0);
            np6.setFormatter(new NumberPicker.Formatter() {
                @Override
                public String format(int value) {
                    return String.format("%02d", value * 15);
                }
            });
        }
    }

    public void showTime(int hour, int min) {
        String format = "PM";

        if (min % 15 != 0) {
            //To make the minutes a multiple of 15.
            min /= 15;
            min += 1;
            min *= 15;
        }
        if (min >= 60) {
            min -= 60;
            hour += 1;
            showTime(hour, min);
        }
        if (hour > 12) {
            hour -= 12;
        }

        visitTime = String.valueOf(new StringBuilder().append(hour).append(":").append(String.format("%02d", min))
                .append(" ").append(format));
        timeButton.setText(new StringBuilder().append(hour).append(":").append(String.format("%02d", min))
                .append(" ").append(format));
    }

    public void showDate(int day, int month) {
        if (this.day == day)
            visitDate = String.valueOf(new StringBuilder().append(day).append("/").append(month_number[month]).append("/").append(year));
        else
            visitDate = String.valueOf(new StringBuilder().append(day).append("/").append(month_number[month]).append("/").append(year_tomorrow));
        dateButton.setText(new StringBuilder().append(day).append("  ").append(month_names[month]));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.guestButton:
                datetimeChooser.setVisibility(view_fragment.VISIBLE);
                break;
            case R.id.timeButton:
                datetimeChooser.setVisibility(view_fragment.VISIBLE);
                break;
            case R.id.dateButton:
                datetimeChooser.setVisibility(view_fragment.VISIBLE);
                break;
            case R.id.setButton:
                np_male = np1.getValue();
                np_female = np2.getValue();
                np_day = np3.getValue();
                np_month = np4.getValue();
                np_hour = np5.getValue();
                np_min = np6.getValue() * 15;
                showDate(dates[np_day], this.month + np_month);
                showTime(np_hour, np_min);
                guestButton.setText(np_male + np_female + " pax");
                maleGuestNumber = String.valueOf(np_male);
                femaleGuestNumber = String.valueOf(np_female);
                datetimeChooser.setVisibility(view_fragment.GONE);
                break;
            case R.id.real_time_deal:

                if (isPocketinActive) {
                    List<AlaCarteCardInfo> list = AlaCarteCardAdapter.CardViewHolder.selected_restaurant_list;
                    if (list.size() != 0) {
                        AlaCarteCardInfo cardInfoRId;
                        JSONObject rest_deal_JsonObject = new JSONObject();

                        for (int i = 0; i < list.size(); i++) {
                            cardInfoRId = list.get(i);
                            try {
                                rest_deal_JsonObject.put(Constants.REST_ID + i, cardInfoRId.restaurant_ID);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //getJsonRId.put(jsonObjectRId);
                        }
                        Log.d("Restaurant_ID_JsonArray", rest_deal_JsonObject.toString());

                        try {
                            rest_deal_JsonObject.put(Constants.MALE_GUEST_NUMBER, maleGuestNumber);
                            rest_deal_JsonObject.put(Constants.FEMALE_GUEST_NUMBER, femaleGuestNumber);
                            rest_deal_JsonObject.put(Constants.DATE_OF_VISIT, visitDate);
                            rest_deal_JsonObject.put(Constants.TIME_OF_VISIT, visitTime);
                        } catch (Exception e) {

                        }

                        //User email fetched from shared prefrences.
                        String email = pref.getString("user_email", null);
                        try {
                            rest_deal_JsonObject.put(Constants.USER_EMAIL_ID, email);
                        } catch (Exception e) {

                        }
                        Log.d("RestDeal_JSONObject", rest_deal_JsonObject.toString());
                        new SenderReceiver(Constants.RESTAURANT_DEAL_REQUEST_SERVER_URL).sendUserDataToServer(rest_deal_JsonObject);

                        Intent intent = new Intent(getContext(), RealTimeDeal.class);
                        intent.putExtra("backactive", false);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), "Select at least 1 restaurant", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast toast=Toast.makeText(getContext(), "Pocketin is active from 11:30 am to 11 pm", Toast.LENGTH_LONG);
                    ViewGroup group = (ViewGroup) toast.getView();
                    TextView messageTextView = (TextView) group.getChildAt(0);
                    messageTextView.setTextSize(14);
                    toast.show();
                }
                break;
        }
    }
}

