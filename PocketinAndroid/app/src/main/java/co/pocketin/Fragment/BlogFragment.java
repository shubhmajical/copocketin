package co.pocketin.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.pocketin.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BlogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class BlogFragment extends android.support.v4.app.Fragment implements View.OnClickListener {

    View view_fragment;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * //@param param1 Parameter 1.
     *  // @param param2 Parameter 2.
     * @return A new instance of fragment BlogFragment.
     */
    public static BlogFragment newInstance() {//(String param1, String param2) {
        BlogFragment fragment = new BlogFragment();
        return fragment;
    }

    public BlogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view_fragment=inflater.inflate(R.layout.fragment_blog, container, false);
        return view_fragment;

    }

    @Override
    public void onClick(View v) {
    }
}
