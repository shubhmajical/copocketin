package co.pocketin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import co.pocketin.GCM.QuickstartPreferences;
import co.pocketin.GCM.RegistrationIntentService;
import co.pocketin.Utilities.Constants;

public class LoginActivity extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    LoginButton loginButton;
    CallbackManager callbackManager;
    AccessTokenTracker accessTokenTracker;
    AccessToken accessToken;
    GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "LoginActivity";
    private Tracker mTracker;
    String name = "Pocketin Login Page";
    public static Bitmap userimage_bitmap;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();

        if (pref.getBoolean("user_profile", false)) {
            callIntent(null);
        }

        // Obtain the shared Tracker instance.
        ServiceCallsApplication application = (ServiceCallsApplication) getApplication();
        mTracker = application.getDefaultTracker();

        // [START google_analytics_hit]
        Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Image~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        // [END google_analytics_hit]

        setContentView(co.pocketin.R.layout.activity_login);

        TextView logo = (TextView) findViewById(co.pocketin.R.id.logo);
        Typeface custom_font = Typeface.createFromAsset(getApplicationContext().getAssets(), "font/keepcalm.ttf");
        logo.setTypeface(custom_font);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PROFILE), new Scope(Scopes.PLUS_LOGIN), new Scope(Scopes.PLUS_ME))
                .requestId()
                .requestProfile()
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();

        //google plus logout
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }

        //Configuring google+ sign in Button.
        SignInButton signInButton = (SignInButton) findViewById(co.pocketin.R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());
        signInButton.setScopes(new Scope[]{Plus.SCOPE_PLUS_LOGIN});
        signInButton.setOnClickListener(this);
        setGooglePlusButtonText(signInButton, "Log in with Google" +
                "        ");

        //Facebook llogin sdk integrating.
        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
                Log.d("LoginActivity", "in access token tracker");

            }
        };


        loginButton = (LoginButton) findViewById(co.pocketin.R.id.login_button);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code
                                Log.d("LoginActivity", "GraphRequestonCompleted--" + response.toString());
                                try {
                                    object.put("social_media_platform", "Facebook");
                                    if(!object.has("email")){
                                        object.put("email", object.get("id")+"@pocketin.co");
                                    }
                                    //userimage_bitmap = getFacebookProfilePicture(object.getString("id"));
                                    Log.d("LoginActivity", "jsonObject--" + object);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                //To send user details to server and to receive AlaCarte card data from server.
                                networkOperation(object);
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d("LoginActivity", "onCancel--");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("LoginActivity", "onError--" + error);
                Toast.makeText(getApplicationContext(), "Check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected void setGooglePlusButtonText(SignInButton signInButton,
                                           String buttonText) {
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setTextSize(15);
                tv.setTypeface(null, Typeface.BOLD);
                tv.setText(buttonText);
                return;
            }
        }
    }

    public static Bitmap getFacebookProfilePicture(final String userID){

        URL imageURL;
        final Bitmap[] bitmap = {null};
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    //Your code goes here
                    Log.d("Userid---", "id--" + userID);
                    try {
            /*
            imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?type=large");
            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
            */
                        URL fb_url = new URL("https://graph.facebook.com/"+userID+"/picture?type=small");//small | noraml | large
                        HttpsURLConnection conn1 = (HttpsURLConnection) fb_url.openConnection();
                        HttpsURLConnection.setFollowRedirects(true);
                        conn1.setInstanceFollowRedirects(true);
                        bitmap[0] = BitmapFactory.decodeStream(conn1.getInputStream());
                        Log.d("Image---", "bit--" + bitmap[0].toString());
                    } catch (Exception e) {
                        Log.d ("Error-", e.toString() +"");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
        return bitmap[0];
    }



    private void callIntent(JSONObject object) {
        Intent intent = new Intent(LoginActivity.this, UserContactNumber.class);
        try {
            intent.putExtra("email_id", object.getString("email"));
        }catch(Exception e){
            e.printStackTrace();
        }
        startActivity(intent);
        finish();
    }

    private void networkOperation(JSONObject object) {

        //Registering User with GCM
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent x) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
            }
        };
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            intent.putExtra("JSONObject from calling Activity/Fragment", object.toString());
            intent.putExtra("URL", Constants.UserInfo_Server_URL);
            startService(intent);
        }

        //new SenderReceiver(Constants.POCKETIN_SERVER_URL).sendUserDataToServer(object);
        setSharedPrefrenceData(object);
        callIntent(object);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("LoginActivity", "onActivityResult");
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        accessTokenTracker.stopTracking();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case co.pocketin.R.id.sign_in_button:
                signIn();
                break;
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void setSharedPrefrenceData(JSONObject object) {
        //To create user session and save data in SharedPrefrences
        try {
            editor.putBoolean("user_profile", true);
            editor.putString("user_id", object.getString("id"));
            editor.putString("user_name", object.getString("name"));
            editor.putString("user_email", object.getString("email"));
            String gender = object.getString("gender");
            editor.putString("user_gender", gender.substring(0, 1).toUpperCase() + gender.substring(1) );
            editor.putString("user_dob", object.getString("birthday"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        editor.commit();

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("Login", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            JSONObject object = new JSONObject();
            try {
                object.put("social_media_platform", "Google Plus");
                object.put("id", acct.getId());
                object.put("name", acct.getDisplayName());
                object.put("email", acct.getEmail());
                if (person.getGender() == 0) {
                    object.put("gender", "male");
                } else {
                    object.put("gender", "female");
                }
                if (person.getBirthday() != null) {
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date birthDate = null;
                    try {
                        birthDate = df.parse(person.getBirthday());
                        df = new SimpleDateFormat("MM/dd/yyyy");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    object.put("birthday", df.format(birthDate));
                }
                Log.d("LoginActivity", "JSONObject--" + object);

                //To send user details to server and to receive AlaCarte card data from server.
                networkOperation(object);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            // Signed out, show unauthenticated UI.
            Log.d("GoogleLogin", "Logout");
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
