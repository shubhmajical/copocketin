package co.pocketin;

import org.json.JSONException;
import org.json.JSONObject;

public class EventCardInfo {
    protected String event_name;
    protected String event_place;
    protected String event_time;
    protected int restaurant_image_id;

    public EventCardInfo(String event_name, String event_place, String event_time, int restaurant_image_id) {
        this.event_name=event_name;
        this.event_place=event_place;
        this.event_time=event_time;
        this.restaurant_image_id=restaurant_image_id;
    }

}
