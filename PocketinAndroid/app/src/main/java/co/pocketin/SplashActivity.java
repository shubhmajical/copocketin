package co.pocketin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.appsflyer.AppsFlyerLib;
import com.quantumgraph.sdk.QG;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import co.pocketin.Utilities.Constants;

public class SplashActivity extends AppCompatActivity {

    private SharedPreferences pref, tut_pref;
    private JSONArray getJsonArray;
    private VolleySingleton mVolleySingleton;
    private RequestQueue mRequestQueue;
    private List<AlaCarteCardInfo> cardData_list;
    private Boolean isInternetConnected;

    //QGraph Variable
    private QG qg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.pocketin.R.layout.activity_splash);
        TextView logo = (TextView) findViewById(co.pocketin.R.id.logo);
        Typeface custom_font = Typeface.createFromAsset(getApplicationContext().getAssets(), "font/keepcalm.ttf");
        logo.setTypeface(custom_font);
        tut_pref = getApplicationContext().getSharedPreferences("TutPref", 0);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode

        //QGraph SDK Initialization
        qg = QG.getInstance(getApplication(), "99ac66fbb491cc826d5f");

        //AppsFlyer SDK Initialization
        AppsFlyerLib.setAppsFlyerKey("AAHq6Hrr9oFAtkRSMChiMo");
        AppsFlyerLib.sendTracking(getApplicationContext());

        if (pref.getBoolean("user_data", false)) {
            isInternetConnected = true;
            getData();
        } else {
            if (!tut_pref.getBoolean("app_install", false)) {
                Intent intent = new Intent(SplashActivity.this, TutorialActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    private void getData() {

        //To receive AlaCarte Card details from server.
        mVolleySingleton = VolleySingleton.getInstance();
        mRequestQueue = mVolleySingleton.getRequestQueue();
        getJsonArray = new JSONArray();

        JsonArrayRequest jsonrequest = new JsonArrayRequest(Request.Method.GET, Constants.RestCardInfo_SERVER_URL, (String) null
                , new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                getJsonArray = response;
                Log.d("ServerResponse", getJsonArray.toString() + " : response ");
                cardData_list = new ArrayList<AlaCarteCardInfo>();
                for (int i = 0; i < getJsonArray.length(); i++) {
                    JSONObject jsonobject = null;
                    try {
                        jsonobject = getJsonArray.getJSONObject(i);
                        String cuisine = jsonobject.getString("cuisineinfo").replace("[", "").replace("]", "").replace("\"", "");
                        cardData_list.add(new AlaCarteCardInfo(jsonobject.getString("restID"), cuisine, jsonobject.getString("rating"), jsonobject.getString("restLocation"), jsonobject.getDouble("longitude"), jsonobject.getDouble("latitude"), jsonobject.getString("restName"), "Cost for two: " + jsonobject.getString("costfortwo"), jsonobject.getString("restImage")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Intent intent = new Intent(SplashActivity.this, BaseActivity.class);
                intent.putExtra("CARD_DETAILS", (Serializable) cardData_list);
                startActivity(intent);
                finish();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ServerError", "error    " + error.toString());
                if (isInternetConnected) {
                    Toast.makeText(getApplicationContext(), "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
                }
                isInternetConnected = false;
                getData();
            }
        });
        mRequestQueue.add(jsonrequest);
    }

    @Override
    public void onStart() {
        super.onStart();
        qg.onStart();
    }
}
