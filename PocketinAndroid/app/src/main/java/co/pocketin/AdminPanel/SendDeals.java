package co.pocketin.AdminPanel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import co.pocketin.RealTimeDeal;
import co.pocketin.RtdCardAdapter;
import co.pocketin.SenderReceiver;
import co.pocketin.Utilities.Constants;

import co.pocketin.R;

/**
 * Created by raharora on 2/22/16.
 */
public class SendDeals extends AppCompatActivity {

    private TextView male_number, female_number, visit_date, visit_time, user_name, rest1, rest2, rest3;
    private EditText deal1, deal2, deal3;
    private Button dealShoot;
    JSONObject jsonObject;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dealsetter);

        male_number = (TextView) findViewById(R.id.male_number);
        female_number = (TextView) findViewById(R.id.female_number);
        visit_date = (TextView) findViewById(R.id.visit_date);
        visit_time = (TextView) findViewById(R.id.visit_time);
        user_name = (TextView) findViewById(R.id.user_name);
        rest1 = (TextView) findViewById(R.id.textview_deal1);
        rest2 = (TextView) findViewById(R.id.textview_deal2);
        rest3 = (TextView) findViewById(R.id.textview_deal3);

        deal1 = (EditText) findViewById(R.id.deal_1);
        deal2 = (EditText) findViewById(R.id.deal_2);
        deal3 = (EditText) findViewById(R.id.deal_3);
        dealShoot = (Button) findViewById(R.id.b_ShootDeal);

        final String jsonString = getIntent().getStringExtra("Restaurant Selected");



        //User ---- > Admin

        try {
            jsonObject = new JSONObject(jsonString);
            Log.d("JsonObjectReceived", jsonObject.toString());
            male_number.setText(jsonObject.getString("male_guest_number") + "M");
            female_number.setText(jsonObject.getString("female_guest_number") + "F");
            visit_date.setText(jsonObject.getString("date_of_request"));
            visit_time.setText(jsonObject.getString("time_of_request"));
            user_name.setText(jsonObject.getString("user_name"));
            rest1.setText(jsonObject.getString("restaurant_name0"));
            rest2.setText(jsonObject.getString("restaurant_name1"));
            rest3.setText(jsonObject.getString("restaurant_name2"));
        } catch (JSONException e) {
            Log.d("UserDeal",jsonString + "");
            e.printStackTrace();
        }

        //Admin ----> User
        try{
            jsonObject = new JSONObject(jsonString);
            String deal0= jsonObject.getString("deal_0");
            //new RealTimeDeal().updateDeals(jsonObject);
            RtdCardAdapter.deals_updation=true;
            RtdCardAdapter.object=jsonObject;
            Intent intent = new Intent(this, RealTimeDeal.class);
            intent.putExtra("backactive", true);
            startActivity(intent);
            //onBackPressed();
            finish();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }


        dealShoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    jsonObject.put("deal_0", deal1.getText());
                    jsonObject.put("deal_1", deal2.getText());
                    jsonObject.put("deal_2", deal3.getText());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("sentDeal-",jsonObject+"");
                Toast.makeText(getApplicationContext(), "Deals Pushed", Toast.LENGTH_SHORT).show();
                new SenderReceiver(Constants.RESTAURANT_DEAL_GIVEN_SERVER_URL).sendUserDataToServer(jsonObject);
            }
        });
    }
}
