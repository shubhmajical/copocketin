package co.pocketin;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by shubham on 26-Dec-15.
 */
public class VolleySingleton {

    private static VolleySingleton volleySingleton_Instance=null;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;


    private VolleySingleton(){
        mRequestQueue = getRequestQueue();

        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    public static synchronized VolleySingleton getInstance(){
        if(volleySingleton_Instance==null)volleySingleton_Instance=new VolleySingleton();
        return volleySingleton_Instance;
    }

    public RequestQueue getRequestQueue(){

        if(mRequestQueue==null) mRequestQueue= Volley.newRequestQueue(ServiceCallsApplication.getAppContext());
        return mRequestQueue;
    }



    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

}
