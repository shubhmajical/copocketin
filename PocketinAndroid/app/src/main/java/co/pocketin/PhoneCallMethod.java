package co.pocketin;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import co.pocketin.Fragment.AlaCarteFragment;

/**
 * Created by shubham on 07-Feb-16.
 */
public class PhoneCallMethod {

    SharedPreferences pref;
    String user_name, userContactNumberStr;

    public void call(final Context context) {

        pref = context.getSharedPreferences("MyPref", 0);
        user_name = pref.getString("user_name", null);
        userContactNumberStr = pref.getString("user_contact_number", null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Confirmation:");
        alertDialogBuilder.setMessage("Our executive will call you within 15 minutes.");
        alertDialogBuilder.setPositiveButton("CONFIRM",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        String routeSmsURL = "http://sms6.routesms.com:8080/bulksms/bulksms?username=pocketin&password=sd34rfgg&type=0&dlr=0&destination=9899941300&source=PCKTIN&message=" + user_name.replace(" ", "%20") + "%20with%20" + userContactNumberStr.replace(" ", "%20") + "%20has%20requested%20you%20to%20call%20back%20for%20some%20query%20in%20the%20next%2015%20minutes.";
                        new SenderReceiver(routeSmsURL).sendSMS();
                        routeSmsURL = "http://sms6.routesms.com:8080/bulksms/bulksms?username=pocketin&password=sd34rfgg&type=0&dlr=0&destination=9910558251&source=PCKTIN&message=" + user_name.replace(" ", "%20") + "%20with%20" + userContactNumberStr.replace(" ", "%20") + "%20has%20requested%20you%20to%20call%20back%20for%20some%20query%20in%20the%20next%2015%20minutes.";
                        new SenderReceiver(routeSmsURL).sendSMS();
                        if(AlaCarteFragment.isPocketinActive) {
                            Toast.makeText(context, "Your request for call is confirmed.", Toast.LENGTH_SHORT).show();
                        }
                        else {

                            Toast toast=Toast.makeText(context, "Pocketin is active from 11:30 am to 11 pm", Toast.LENGTH_LONG);
                            ViewGroup group = (ViewGroup) toast.getView();
                            TextView messageTextView = (TextView) group.getChildAt(0);
                            messageTextView.setTextSize(14);
                            toast.show();
                        }
                    }
                });
        alertDialogBuilder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void cancelDeal(final Context context, final String username, final String restaurant_name, final String visit_date, final String visit_time) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Confirmation:");
        alertDialogBuilder.setMessage("Are you sure you want to cancel this amazing deal:");
        alertDialogBuilder.setPositiveButton("CONFIRM",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        String routeSmsURL = "http://sms6.routesms.com:8080/bulksms/bulksms?username=pocketin&password=sd34rfgg&type=0&dlr=0&destination=9899941300&source=PCKTIN&message=This%20is%20to%20inform%20you%20that%20" + username.replace(" ", "%20") + "%20who%20had%20booked%20a%20table%20at%20" + restaurant_name.replace(" ", "%20") + "%20on%20" + visit_date.replace(" ", "%20") + "%20at%20" + visit_time.replace(" ", "%20") + "%20has%20cancelled%20the%20booking.%20Call%20rest%20and%20inform.";
                        new SenderReceiver(routeSmsURL).sendSMS();
                        routeSmsURL = "http://sms6.routesms.com:8080/bulksms/bulksms?username=pocketin&password=sd34rfgg&type=0&dlr=0&destination=9910558251&source=PCKTIN&message=This%20is%20to%20inform%20you%20that%20" + username.replace(" ", "%20") + "%20who%20had%20booked%20a%20table%20at%20" + restaurant_name.replace(" ", "%20") + "%20on%20" + visit_date.replace(" ", "%20") + "%20at%20" + visit_time.replace(" ", "%20") + "%20has%20cancelled%20the%20booking.%20Call%20rest%20and%20inform.";
                        new SenderReceiver(routeSmsURL).sendSMS();
                        Toast.makeText(context, "Your reservation has been cancelled.", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                });
        alertDialogBuilder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
