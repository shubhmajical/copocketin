package co.pocketin;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import co.pocketin.Utilities.Constants;

public class RtdCardAdapter extends RecyclerView.Adapter<RtdCardAdapter.CardViewHolder> {

    public List<AlaCarteCardInfo> list;
    int listSize;
    public static JSONObject object;
    int min = 4, sec = 60;
    int flag = 1;
    ImageLoader imageLoader;
    public static CountDownTimer timer;
    public static boolean deals_updation = false;
    //Url of the image to be passed to Confirmation Page
    public static String restaurant_image_url;
    private Context context;

    public RtdCardAdapter(List<AlaCarteCardInfo> list, JSONObject object, Context context) {
        deals_updation = true;
        this.list = list;
        this.object = object;
        this.context = context;
    }

    public RtdCardAdapter(List<AlaCarteCardInfo> list, Context context) {
        this.list = list;
        listSize = list.size();
        this.context = context;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(co.pocketin.R.layout.cardview_rtd, viewGroup, false);
        return new CardViewHolder(itemView, context);
    }

    @Override
    public void onBindViewHolder(final CardViewHolder holder, final int position) {

        AlaCarteCardInfo ci = list.get(position);
        holder.rating.setText(ci.rating);
        holder.location.setText(ci.location);
        holder.restaurant_name.setText(ci.restaurant_name);
        holder.costfortwo.setText(ci.costfortwo);
        holder.getthisdeal.setClickable(false);
        restaurant_image_url = ci.restaurant_image_url;

        if (!deals_updation) {
            timer = new CountDownTimer(300000, 1000) {

                public void onTick(long millisUntilFinished) {
                    //min= (int) (millisUntilFinished / 60000);
                    if (sec == 0) {
                        min--;
                        sec = 60;
                    }
                    flag = (position + 1) / listSize;
                    sec = sec - flag;
                    //sec= (int) (millisUntilFinished/((min+1)*1000));
                    holder.getthisdeal.setText(min + " : " + sec + " minutes remaining ");
                }

                public void onFinish() {
                    holder.getthisdeal.setText("GET THIS DEAL");
                }

            }.start();
        } else {
            holder.getthisdeal.setText("GET THIS DEAL");
            holder.getthisdeal.setClickable(true);
            try {
                if (position == 0) {
                    holder.fetch_deal.setText(object.getString("deal_0"));
                    holder.dealProposed = object.getInt("deal_proposed0");
                } else if (position == 1) {
                    holder.fetch_deal.setText(object.getString("deal_1"));
                    holder.dealProposed = object.getInt("deal_proposed1");
                } else {
                    holder.fetch_deal.setText(object.getString("deal_2"));
                    holder.dealProposed = object.getInt("deal_proposed2");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(List<AlaCarteCardInfo> list, JSONObject object) {
        deals_updation = true;
        this.list = list;
        this.object = object;
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {

        protected TextView rating;
        protected TextView location;
        protected TextView restaurant_name;
        protected NetworkImageView restaurant_image;
        protected TextView fetch_deal;
        protected TextView costfortwo;
        protected Button getthisdeal;
        protected int dealProposed;

        public CardViewHolder(View itemView, final Context context) {
            super(itemView);

            rating = (TextView) itemView.findViewById(co.pocketin.R.id.rating);
            location = (TextView) itemView.findViewById(co.pocketin.R.id.location);
            restaurant_name = (TextView) itemView.findViewById(co.pocketin.R.id.restaurant_name);
            restaurant_image = (NetworkImageView) itemView.findViewById(co.pocketin.R.id.restaurant_network_image);
            fetch_deal = (TextView) itemView.findViewById(co.pocketin.R.id.fetch_deal);
            costfortwo = (TextView) itemView.findViewById(co.pocketin.R.id.costfortwo);
            getthisdeal = (Button) itemView.findViewById(co.pocketin.R.id.get_this_deal_button);


            getthisdeal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    VolleySingleton mVolleySingleton = VolleySingleton.getInstance();
                    final RequestQueue mRequestQueue = mVolleySingleton.getRequestQueue();
                    final JSONObject rest_obj = new JSONObject();
                    try {
                        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
                        rest_obj.put("RESTAURANT_NAME", restaurant_name.getText());
                        rest_obj.put("LOCATION", location.getText());
                        rest_obj.put("FETCHED_DEAL", fetch_deal.getText());
                        rest_obj.put("Deal_Proposed", dealProposed);
                        rest_obj.put("RESTAURANT_IMAGE", restaurant_image_url);
                        rest_obj.put("MALE_GUEST_NUMBER", object.getString("male_guest_number"));
                        rest_obj.put("FEMALE_GUEST_NUMBER", object.getString("female_guest_number"));
                        rest_obj.put("VISIT_DATE", object.getString("date_of_request"));
                        rest_obj.put("VISIT_TIME", object.getString("time_of_request"));
                        rest_obj.put("UserEmail", pref.getString("user_email", null));
                        rest_obj.put("DealTimeID", pref.getString("DealTimeID", null));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setTitle("Coupon Verification");
                    alertDialogBuilder.setMessage("Enter promo code for cashback");
                    final EditText input = new EditText(context);
                    input.setGravity(Gravity.CENTER);
                    input.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    input.setSingleLine(true);
                    final TextView result = new TextView(context);
                    result.setGravity(Gravity.CENTER);
                    result.setText("");
                    result.setTextColor(Color.RED);
                    LinearLayout lay = new LinearLayout(context);
                    lay.setOrientation(LinearLayout.VERTICAL);
                    lay.addView(input);
                    lay.addView(result);
                    alertDialogBuilder.setView(lay);
                    alertDialogBuilder.setNegativeButton("Skip",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    try {
                                        rest_obj.put("PROMO_CODE", "NO_PROMO");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    Log.d("json_object", rest_obj.toString());
                                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constants.DEAL_GRABBED_BY_USER_URL, rest_obj, new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            Log.d("response", response.toString());
                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.d("error", " inside error");
                                        }
                                    });
                                    mRequestQueue.add(jsonObjectRequest);
                                    Intent intent = new Intent(v.getContext(), RtdConfirmation.class);
                                    intent.putExtra("CARD_DETAIL", rest_obj.toString());
                                    v.getContext().startActivity(intent);
                                }
                            });
                    alertDialogBuilder.setPositiveButton("Confirm",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                }
                            });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            final Boolean[] wantToCloseDialog = {false};

                            final String value = input.getText().toString().trim();
                            try {
                                rest_obj.put("PROMO_CODE", value);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d("json_object", rest_obj.toString());
                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constants.DEAL_GRABBED_BY_USER_URL, rest_obj, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d("response", response.toString());
                                    try {
                                        wantToCloseDialog[0] = response.getBoolean("response");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if (wantToCloseDialog[0]) {
                                        alertDialog.dismiss();
                                        Toast toast=Toast.makeText(context, "Your promo code has been applied successfully", Toast.LENGTH_LONG);
                                        ViewGroup group = (ViewGroup) toast.getView();
                                        TextView messageTextView = (TextView) group.getChildAt(0);
                                        messageTextView.setTextSize(13);
                                        toast.show();
                                        Intent intent = new Intent(v.getContext(), RtdConfirmation.class);
                                        intent.putExtra("CARD_DETAIL", rest_obj.toString());
                                        v.getContext().startActivity(intent);
                                    } else {
                                        result.setText("This promo code is invalid.");
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("error", " inside error");
                                }
                            });
                            mRequestQueue.add(jsonObjectRequest);

                        }
                    });

                }

            });
        }
    }
}
