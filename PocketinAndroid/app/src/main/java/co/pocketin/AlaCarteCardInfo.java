package co.pocketin;

import android.graphics.drawable.Drawable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by shubham on 24-Dec-15.
 */
public class AlaCarteCardInfo implements Serializable {
    public String restaurant_ID;
    protected String cuisine;
    protected String rating;
    protected String location;
    public double longitude;
    public double latitude;
    public String restaurant_name;
    protected String costfortwo;
    protected String restaurant_image_url;

    public AlaCarteCardInfo(String restaurant_ID, String cuisine, String rating, String location, double longitude, double latitude, String restaurant_name, String costfortwo, String restaurant_image_url) {
        this.restaurant_ID=restaurant_ID;
        this.cuisine=cuisine;
        this.rating=rating;
        this.location=location;
        this.longitude=longitude;
        this.latitude=latitude;
        this.restaurant_name=restaurant_name;
        this.costfortwo=costfortwo;
        this.restaurant_image_url= restaurant_image_url;
    }
}
