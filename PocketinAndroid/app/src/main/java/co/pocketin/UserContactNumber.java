package co.pocketin;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import co.pocketin.Utilities.Constants;

public class UserContactNumber extends AppCompatActivity {

    private CoordinatorLayout coordinatorLayout;
    private EditText inputContactNumber;
    private TextInputLayout inputLayoutContactNumber;
    private Button btnConfirm;
    SharedPreferences pref; // 0 - for private mode
    SharedPreferences.Editor editor;
    private String strContactNumber;
    private String pin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.pocketin.R.layout.activity_user_contact_number);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();

        TextView logo= (TextView) findViewById(co.pocketin.R.id.logo);
        Typeface custom_font = Typeface.createFromAsset(getApplicationContext().getAssets(), "font/keepcalm.ttf");
        logo.setTypeface(custom_font);

        coordinatorLayout = (CoordinatorLayout) findViewById(co.pocketin.R.id
                .coordinatorLayout);

        inputLayoutContactNumber
                = (TextInputLayout) findViewById(co.pocketin.R.id.input_layout_contact_number);
        inputContactNumber = (EditText) findViewById(co.pocketin.R.id.user_contact_number);

        inputContactNumber.addTextChangedListener(new MyTextWatcher(inputContactNumber));

        btnConfirm = (Button) findViewById(co.pocketin.R.id.btn_confirm);

        btnConfirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                strContactNumber = inputContactNumber.getText().toString();
                if (!validateNumber()) {
                    return;
                } else {
                    pin=generatePIN();
                    String routeSmsURL = "http://sms6.routesms.com:8080/bulksms/bulksms?username=pocketin&password=sd34rfgg&type=0&dlr=0&destination=" + strContactNumber + "&source=PCKTIN&message=Welcome%20to%20Pocketin.%20Please%20enter%20the%20OTP%20" + pin + "%20to%20complete%20the%20verification%20of%20your%20number.%20Keep%20Pocketin!";
                    new SenderReceiver(routeSmsURL).sendSMS();
                    checkOTP();
                }
            }
        });
    }

    private String generatePIN()
    {
        //generate a 4 digit integer 1000 <10000
        int randomPIN = (int)(Math.random()*9000)+1000;
        return String.valueOf(randomPIN);
    }

    private void checkOTP() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Verification");
        alertDialogBuilder.setMessage("Enter the OTP sent to your mobile:");
        final EditText input = new EditText(this);
        input.setGravity(Gravity.CENTER);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        alertDialogBuilder.setView(input);
        alertDialogBuilder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        String value = input.getText().toString().trim();
                        if (value.equals(pin)) {
                            submitForm();
                        } else {
                            Toast.makeText(getApplicationContext(), "OTP is incorrect", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    private void submitForm() {
        // Snackbar
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Welcome to PocketIn Merchant Application", Snackbar.LENGTH_LONG);

        snackbar.show();

        //Mapping input from the fragment into a Hashmap to be later converted to JSon
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.USER_CONTACT_NUMBER, strContactNumber);

        Log.d("Login Input", "UserContactNumber" + params.get(Constants.USER_CONTACT_NUMBER));

        //Service call to PocketinBackend to update Restaurant Info table
        String jsonString = getIntent().getStringExtra("email_id");
        params.put(Constants.USER_EMAIL_ID,jsonString);
        Log.d("rahul", "rahul" + params);
        new SenderReceiver(Constants.UserPhone_Server_URL).sendToServer(params);

        editor.putBoolean("user_data", true);
        editor.putString("user_contact_number", strContactNumber);
        editor.commit();
        Intent intent = new Intent(UserContactNumber.this, SplashActivity.class);
        startActivity(intent);
        finish();
    }



    private boolean validateNumber() {
        if (inputContactNumber.getText().toString().trim().isEmpty()) {
            inputLayoutContactNumber.setError(getString(co.pocketin.R.string.err_msg_name));
            requestFocus(inputContactNumber);
            return false;
        }
        else if(inputContactNumber.getText().toString().trim().length()!=10){
            inputLayoutContactNumber.setError(getString(co.pocketin.R.string.err_msg_name));
            requestFocus(inputContactNumber);
            return false;
        }
        else {
            inputLayoutContactNumber.setErrorEnabled(false);
        }
        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        public MyTextWatcher(View view) {
            this.view = view;

        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case co.pocketin.R.id.user_contact_number:
                    //validateNumber();
                    break;
            }
        }
    }
}
