package co.pocketin;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class EventCardAdapter extends RecyclerView.Adapter<EventCardAdapter.CardViewHolder> {

    public static List<EventCardInfo> list;

    public EventCardAdapter(List<EventCardInfo> list){
        this.list=list;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(co.pocketin.R.layout.cardview_event, viewGroup, false);

        return new CardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {

        EventCardInfo ci = list.get(position);
        holder.event_name.setText(ci.event_name);
        holder.event_place.setText(ci.event_place);
        holder.event_time.setText(ci.event_time);
        holder.restaurant_image.setImageResource(ci.restaurant_image_id);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {

        protected TextView event_name;
        protected TextView event_place;
        protected TextView event_time;
        protected ImageView restaurant_image;

        public static List<EventCardInfo> select_list= new ArrayList<>();


        public CardViewHolder(View itemView) {
            super(itemView);
            event_name= (TextView) itemView.findViewById(co.pocketin.R.id.event_name);
            event_place= (TextView) itemView.findViewById(co.pocketin.R.id.event_place);
            event_time= (TextView) itemView.findViewById(co.pocketin.R.id.event_time);
            restaurant_image= (ImageView) itemView.findViewById(co.pocketin.R.id.restaurant_image);

        }
    }

}
