package co.pocketin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import co.pocketin.AdminPanel.SendDeals;

public class RealTimeDeal extends AppCompatActivity {

    RecyclerView recList;
    LinearLayoutManager llm;
    RtdCardAdapter card_adapter;
    List<AlaCarteCardInfo> list;
    Boolean isBackActive;
    private BroadcastReceiver mMyBroadcastReceiver;
    public static boolean realtimedealActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.pocketin.R.layout.activity_real_time_deal);
        isBackActive = getIntent().getExtras().getBoolean("backactive");
        Toolbar toolbar = (Toolbar) findViewById(co.pocketin.R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        recList = (RecyclerView) findViewById(co.pocketin.R.id.cardList);

        list = AlaCarteCardAdapter.CardViewHolder.selected_restaurant_list;
        card_adapter = new RtdCardAdapter(list, this);
        recList.setAdapter(card_adapter);
        recList.setHasFixedSize(true);
        llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(isBackActive) {
            super.onBackPressed();
            Arrays.fill(AlaCarteCardAdapter.isSelected, Boolean.FALSE);
            AlaCarteCardAdapter.CardViewHolder.selected_restaurant_list.clear();
            RtdCardAdapter.deals_updation=false;
            Intent i = new Intent(this, SplashActivity.class);
            startActivityForResult(i, 1);
            finish();
        }
        else {
            Toast.makeText(this, "Please wait until deals appear.", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateDeals(JSONObject object) {
        card_adapter.timer.cancel();
        card_adapter= new RtdCardAdapter(AlaCarteCardAdapter.CardViewHolder.selected_restaurant_list, object, this);
        recList.setAdapter(card_adapter);
        card_adapter.notifyDataSetChanged();
    }

    /* Updating the already running instance of this class with the Deals Recieved */

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);//must store the new intent unless getIntent() will return the old one
        processExtraData();
    }

    private void processExtraData(){
        Intent intent = getIntent();
        //use the data received here
        isBackActive = getIntent().getExtras().getBoolean("backactive");
        card_adapter = new RtdCardAdapter(list, this);
        recList.setAdapter(card_adapter);
        recList.setHasFixedSize(true);
        llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
    }

    @Override
    protected void onStart() {
        super.onStart();
        realtimedealActive =true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        realtimedealActive = true;
        mMyBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                // Here you can refresh your listview or other UI
                String dealValues =  intent.getExtras().getString("deals");
                Toast.makeText(context, "New Deal", Toast.LENGTH_SHORT).show();
                /*Intent call_intent = new Intent(context, SendDeals.class);
                intent.putExtra("Restaurant Selected", dealValues);
                startActivity(call_intent);*/

                try{
                    JSONObject jsonObject = new JSONObject(dealValues);
                    RtdCardAdapter.deals_updation=true;
                    RtdCardAdapter.object=jsonObject;
                    Intent reset_intent = getIntent();
                    finish();
                    reset_intent.putExtra("backactive", true);
                    startActivity(reset_intent);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        };
        try {
            LocalBroadcastManager.getInstance(this).registerReceiver(mMyBroadcastReceiver, new IntentFilter("my_intent"));

        } catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        realtimedealActive = false;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMyBroadcastReceiver);
    }

}
