package co.pocketin;

public class BuffetCardInfo {
    protected String cost_per_head;
    protected String cuisines;
    protected String restaurant_location;
    protected int restaurant_image_id;

    public BuffetCardInfo(String cost_per_head, String cuisines, String restaurant_location, int restaurant_image_id) {
        this.cost_per_head=cost_per_head;
        this.cuisines=cuisines;
        this.restaurant_location=restaurant_location;
        this.restaurant_image_id=restaurant_image_id;
    }

}
