package co.pocketin;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.List;


public class AlaCarteCardAdapter extends RecyclerView.Adapter<AlaCarteCardAdapter.CardViewHolder> {

    private static List<AlaCarteCardInfo> list;
    public static boolean[] isSelected;
    ImageLoader mImageLoader;
    private Context context;

    public AlaCarteCardAdapter(List<AlaCarteCardInfo> list, Context context) {
        this.list = list;
        this.context = context;
        isSelected = new boolean[list.size()];
        mImageLoader = VolleySingleton.getInstance().getImageLoader();

    }


    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(co.pocketin.R.layout.cardview_alacarte, viewGroup, false);
        return new CardViewHolder(itemView, context);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {

        AlaCarteCardInfo ci = list.get(position);
        holder.cuisine.setText(ci.cuisine);
        holder.rating.setText(ci.rating);
        holder.location.setText(ci.location);
        holder.restaurant_name.setText(ci.restaurant_name);
        holder.costfortwo.setText(ci.costfortwo);
        holder.restaurant_image.setImageUrl(ci.restaurant_image_url, mImageLoader);

        if (isSelected[position]) {
            holder.select_restaurant.setTextColor(co.pocketin.R.color.green);
            holder.select_restaurant.setText("SELECTED");
        } else {
            holder.select_restaurant.setTextColor(Color.BLACK);
            holder.select_restaurant.setText("SELECT");
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateListData(List<AlaCarteCardInfo> filteredList) {
        list.clear();
        list.addAll(filteredList);
        notifyDataSetChanged();
    }

    public static void setList(List<AlaCarteCardInfo> up_list) {
        list = up_list;
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {

        protected TextView cuisine;
        protected TextView rating;
        protected TextView location;
        protected TextView restaurant_name;
        protected TextView costfortwo;
        protected NetworkImageView restaurant_image;
        protected Button select_restaurant;

        public static List<AlaCarteCardInfo> selected_restaurant_list = new ArrayList<>();

        public CardViewHolder(View itemView, final Context context) {
            super(itemView);
            cuisine = (TextView) itemView.findViewById(co.pocketin.R.id.cuisine);
            rating = (TextView) itemView.findViewById(co.pocketin.R.id.rating);
            location = (TextView) itemView.findViewById(co.pocketin.R.id.location);
            restaurant_name = (TextView) itemView.findViewById(co.pocketin.R.id.restaurant_name);
            costfortwo = (TextView) itemView.findViewById(co.pocketin.R.id.costfortwo);
            restaurant_image = (NetworkImageView) itemView.findViewById(co.pocketin.R.id.restaurant_image);
            select_restaurant = (Button) itemView.findViewById(co.pocketin.R.id.select_restaurant);
            select_restaurant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select_restaurant.getText().equals("SELECTED")) {
                        select_restaurant.setTextColor(Color.BLACK);
                        select_restaurant.setText("SELECT");
                        isSelected[getPosition()] = false;
                        selected_restaurant_list.remove(list.get(getPosition()));
                    } else {
                        if (selected_restaurant_list.size() < 3) {
                            select_restaurant.setTextColor(co.pocketin.R.color.green);
                            select_restaurant.setText("SELECTED");
                            isSelected[getPosition()] = true;
                            selected_restaurant_list.add(list.get(getPosition()));
                            Toast.makeText(context, "You can select " + (3 - selected_restaurant_list.size()) + " more restaurants.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Now, Click on RealTime Deal button.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }

}
