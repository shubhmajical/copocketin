package co.pocketin;

/**
 * Created by shubham on 24-Dec-15.
 */
public class RtdCardInfo {

    protected String rating;
    protected String location;
    protected String restaurant_name;
    protected String costfortwo;

    public RtdCardInfo(String rating, String location, String restaurant_name, String costfortwo) {

        this.rating=rating;
        this.location=location;
        this.restaurant_name=restaurant_name;
        this.costfortwo=costfortwo;
    }

}
