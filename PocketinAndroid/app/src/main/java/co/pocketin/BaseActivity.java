package co.pocketin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import co.pocketin.AutoComplete_Places.PlacesAutoCompleteAdapter;
import co.pocketin.AutoComplete_Places.RecyclerItemClickListener;
import co.pocketin.Fragment.AlaCarteFragment;
import co.pocketin.Utilities.Constants;
import co.pocketin.Utilities.SlidingTabLayout;
import de.hdodenhof.circleimageview.CircleImageView;

import com.quantumgraph.sdk.QG;

/**
 * Main Activity which is called after successful login of the user.
 * Contains code for setting toolbar and tabs.
 */

public class BaseActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private TextView location_tag;
    private Spinner location;
    private Intent intent;
    private List<AlaCarteCardInfo> list;
    private ImageButton phone_call;
    private TextView name_user, gender_user, age_user;
    private CircleImageView pic_user;
    private SharedPreferences pref;
    public ArrayList<Fragment> fragmentlist_instances = new ArrayList<>();

    //Google Places AutoComplete
    protected GoogleApiClient mGoogleApiClient;

    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(
            new LatLng(28.4700, 77.0300), new LatLng(28.6700, 77.4200));

    private EditText mAutocompleteView;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    ImageView delete;

    protected void onCreate(Bundle savedInstanceState) {
        //to restore some saved state
        super.onCreate(savedInstanceState);
        setContentView(co.pocketin.R.layout.activity_base);

        list = (ArrayList<AlaCarteCardInfo>) getIntent().getSerializableExtra("CARD_DETAILS");

        //to set Google Places API for AutoComplete of Location field of Restaurants
        setUpGooglePlacesAutoComplete();

        //to set navigation drawer on the toolbar
        setUpNavigationDrawer();

        //to set tabs on the footer
        setTabs();

        location_tag = (TextView) findViewById(R.id.location_tag);
        Typeface custom_font = Typeface.createFromAsset(getApplicationContext().getAssets(), "font/Copperplate Gothic Light Regular.ttf");
        location_tag.setTypeface(custom_font);

        phone_call = (ImageButton) findViewById(co.pocketin.R.id.phone_call);
        phone_call.setOnClickListener(this);

    }

    private void setUpGooglePlacesAutoComplete() {

        buildGoogleApiClient();
        mAutocompleteView = (EditText) findViewById(R.id.autocomplete_places);

        delete = (ImageView) findViewById(R.id.cross);

        mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(this, R.layout.searchview_adapter,
                mGoogleApiClient, BOUNDS_INDIA, null);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mAutoCompleteAdapter);
        delete.setOnClickListener(this);
        mAutocompleteView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mAutocompleteView.setCursorVisible(true);
            }
        });
        mAutocompleteView.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (!s.toString().equals("") && mGoogleApiClient.isConnected()) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    delete.setVisibility(View.VISIBLE);
                    mRecyclerView.bringToFront();
                    mAutoCompleteAdapter.getFilter().filter(s.toString());
                } else if (!mGoogleApiClient.isConnected()) {
                    //Toast.makeText(getApplicationContext(), Constants.API_NOT_CONNECTED,Toast.LENGTH_SHORT).show();
                    Log.e(Constants.PlacesTag, Constants.API_NOT_CONNECTED);
                } else {
                    mRecyclerView.setVisibility(View.INVISIBLE);
                    delete.setVisibility(View.INVISIBLE);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        final PlacesAutoCompleteAdapter.PlaceAutocomplete item = mAutoCompleteAdapter.getItem(position);
                        final String placeId = String.valueOf(item.placeId);
                        Log.i("TAG", "Autocomplete item selected: " + item.description);
                        /*
                             Issue a request to the Places Geo Data API to retrieve a Place object with additional details about the place.
                         */

                        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                                .getPlaceById(mGoogleApiClient, placeId);
                        placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                            @Override
                            public void onResult(PlaceBuffer places) {
                                if (places.getCount() == 1) {
                                    //Do the things here on Click.....
                                    String place = (String) item.description;
                                    if (place.contains(",")) {
                                        mAutocompleteView.setText(place.substring(0, place.indexOf(",")));
                                    } else {
                                        mAutocompleteView.setText(place);
                                    }
                                    mRecyclerView.setVisibility(View.INVISIBLE);
                                    mAutocompleteView.setCursorVisible(false);
                                    String s = String.valueOf(places.get(0).getLatLng());
                                    Log.d("Lat/Long-", s + " lat-->" + s.substring(s.indexOf("(") + 1, s.indexOf(",")) + " lng-->" + s.substring(s.indexOf(",") + 1, s.indexOf(")")));
                                    getCirclePoints(s.substring(s.indexOf("(") + 1, s.indexOf(",")), s.substring(s.indexOf(",") + 1, s.indexOf(")")));
                                    //To hide the keyboard
                                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                                } else {
                                    //Toast.makeText(getApplicationContext(), Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        Log.i("TAG", "Clicked: " + item.description);
                        Log.i("TAG", "Called getPlaceById to get Place details for " + item.placeId);
                    }
                })
        );
    }

    public void getCirclePoints(String x0, String y0) {
        float[] results = new float[1];
        List<AlaCarteCardInfo> updatedata = new ArrayList<>();
        AlaCarteCardInfo obj;
        try {
            for (int i = 0; i < list.size(); i++) {
                obj = list.get(i);
                Location.distanceBetween(Double.parseDouble(x0), Double.parseDouble(y0), obj.latitude, obj.longitude, results);
                float distanceInMeters = results[0];
                boolean isWithin4km = distanceInMeters < 4000;
                if (isWithin4km) {
                    updatedata.add(obj);
                    Log.d("FilteredRestName", obj.restaurant_name + "," + obj.location + "," + distanceInMeters);
                }
            }
            if (updatedata.size() == 0) {
                Toast.makeText(this, "No restaurants found in this location", Toast.LENGTH_SHORT).show();
            }
            Fragment fragment = fragmentlist_instances.get(0);
            if (fragment instanceof AlaCarteFragment) {

                ((AlaCarteFragment) fragment).card_adapter.setList(updatedata);
                ((AlaCarteFragment) fragment).card_adapter.notifyDataSetChanged();
            }
            Log.d("listsize--", "actual-" + list.size() + "searched-" + updatedata.size());
        } catch (Exception e) {
            Log.d("Exception", e.toString() + "");
        }

        //System.out.println("Longitude: " + foundLongitude + "  Latitude: " + foundLatitude );
    }


    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof AlaCarteFragment) {
            fragmentlist_instances.add(0, fragment);
        }
    }

    private void setUpNavigationDrawer() {

        Toolbar toolbar = (Toolbar) findViewById(co.pocketin.R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(co.pocketin.R.id.navigation_view);
        View header = navigationView.getHeaderView(0);
        pic_user = (CircleImageView) header.findViewById(R.id.profile_image_user);
        name_user = (TextView) header.findViewById(co.pocketin.R.id.name_user);
        gender_user = (TextView) header.findViewById(co.pocketin.R.id.gender_user);
        //age_user = (TextView) header.findViewById(co.pocketin.R.id.age_user);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        //pic_user.setImageBitmap(LoginActivity.getFacebookProfilePicture(pref.getString("user_id", null)));
        name_user.setText(pref.getString("user_name", null));
        gender_user.setText(pref.getString("user_gender", null));
        /*
        //CODE for displaying age of user.

        Log.d("dob", " " + pref.getString("user_dob", null));
        if (pref.getString("user_dob", "null").equals("null"))
            age_user.setText("-");
        else
            age_user.setText("" + getAge(pref.getString("user_dob", null)));
        */

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {


                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                //Closing drawer on item click
                drawerLayout.closeDrawers();

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    /*case R.id.history:
                        intent = new Intent(BaseActivity.this, History.class);
                        startActivity(intent);
                        return true;*/
                    case co.pocketin.R.id.share:
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, "Well, i figured out that the Real Time Deal " +
                                "Discount Engine at Pocketin gives me the best discount and deals when I dine out" +
                                " along with plenty of other cool features. Download- https://play.google.com/store/apps/details?id=co.pocketin&hl=en");

                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                        return true;
                    case co.pocketin.R.id.group_bookings:
                        intent = new Intent(BaseActivity.this, GroupBookings.class);
                        startActivity(intent);
                        return true;
                    case co.pocketin.R.id.terms_and_conditions:
                        intent = new Intent(BaseActivity.this, TermsAndCondition.class);
                        startActivity(intent);
                        return true;
                    case co.pocketin.R.id.logout:

                        //Facebook logout
                        LoginManager.getInstance().logOut();
                        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
                        pref.edit().clear().commit();
                        intent = new Intent(BaseActivity.this, SplashActivity.class);
                        startActivity(intent);
                        finish();
                        return true;
                    default:
                        Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                        return true;

                }
            }
        });

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(co.pocketin.R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, co.pocketin.R.string.openDrawer, co.pocketin.R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }


    private void setTabs() {
        ViewPager vpPager = (ViewPager) findViewById(co.pocketin.R.id.vpPager);
        ContentFragmentAdapter adapterViewPager = new ContentFragmentAdapter(getSupportFragmentManager(), this, list);
        vpPager.setAdapter(adapterViewPager);

        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(co.pocketin.R.id.sliding_tabs);
        slidingTabLayout.setBackgroundColor(getResources().getColor(co.pocketin.R.color.tab_color));
        slidingTabLayout.setTextColor(getResources().getColor(co.pocketin.R.color.tab_text_color));
        slidingTabLayout.setTextColorSelected(getResources().getColor(co.pocketin.R.color.tab_text_color_selected));
        //slidingTabLayout.setBackground(R.drawable.ic_launcher);
        slidingTabLayout.setDistributeEvenly();
        slidingTabLayout.setViewPager(vpPager);
        slidingTabLayout.setTabSelected(0);

        // Change indicator color
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(co.pocketin.R.color.tab_indicator);
            }
        });

    }

    private int getAge(String dob_user) {
        Calendar dob = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Date birthDate = null;
        try {
            birthDate = df.parse(dob_user);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dob.setTime(birthDate);
        Calendar today = Calendar.getInstance();
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
            age--;
        } else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
                && today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
            age--;
        }
        return age;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == co.pocketin.R.id.phone_call) {
            new PhoneCallMethod().call(this);
        }
        if (v == delete) {
            mAutocompleteView.setText("");
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.v("Google API Callback", "Connection Done");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v("Google API Callback", "Connection Suspended");
        Log.v("Code", String.valueOf(i));
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v("Google API Callback", "Connection Failed");
        Log.v("Error Code", String.valueOf(connectionResult.getErrorCode()));
        Toast.makeText(this, Constants.API_NOT_CONNECTED, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
            Log.v("Google API", "Connecting");
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            Log.v("Google API", "Dis-Connecting");
            mGoogleApiClient.disconnect();
        }
    }

}
