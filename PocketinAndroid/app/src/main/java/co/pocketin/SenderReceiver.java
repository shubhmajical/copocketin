package co.pocketin;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;


public class SenderReceiver {

    private JSONArray getJsonArray;
    private VolleySingleton mVolleySingleton;
    private RequestQueue mRequestQueue;
    private static String server_url;


    public SenderReceiver(String server_url) {
        this.server_url = server_url;
    }


    //Method for receiving data from server in form of json array.
    public JSONArray receiveFromServer() {
        mVolleySingleton = VolleySingleton.getInstance();
        mRequestQueue = mVolleySingleton.getRequestQueue();
        getJsonArray = new JSONArray();

        JsonArrayRequest jsonrequest = new JsonArrayRequest(Request.Method.GET, server_url, (String) null
                , new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                getJsonArray = response;
                Log.d("ServerResponse", getJsonArray.toString() + " : response ");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ServerError", "error    " + error.toString());
            }
        });
        mRequestQueue.add(jsonrequest);
        return getJsonArray;
    }


    public void sendToServer(final HashMap params) {
        mVolleySingleton = VolleySingleton.getInstance();
        mRequestQueue = mVolleySingleton.getRequestQueue();

        Log.d("Rahul params ",""+new JSONObject(params));

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, this.server_url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", " inside error");
                //sendToServer(data);
            }
        });
        Log.d("Rahul JSONRequest ",""+jsonObjectRequest);
        Log.d("Rahul JSONRequest ",""+jsonObjectRequest.getMethod());
        Log.d("Rahul JSONRequest ",""+jsonObjectRequest.getUrl());
        Log.d("Rahul JSONRequest ",""+jsonObjectRequest.getBody());
        Log.d("Rahul JSONRequest ",""+jsonObjectRequest.getOriginUrl());
        Log.d("Rahul JSONRequest ",""+jsonObjectRequest.getBodyContentType());
        Log.d("Rahul JSONRequest ",""+jsonObjectRequest.getIdentifier());
        Log.d("Rahul JSONRequest ",""+jsonObjectRequest.getTimeoutMs());


        //Service Call to PocketinService
        mRequestQueue.add(jsonObjectRequest);
    }

    public void sendUserDataToServer(final JSONObject jsonObject) {
        mVolleySingleton = VolleySingleton.getInstance();
        mRequestQueue = mVolleySingleton.getRequestQueue();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, this.server_url, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("response", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", " inside error");
                //sendUserDataToServer(data);
            }
        });
        //Service Call to PocketinService
        mRequestQueue.add(jsonObjectRequest);
    }

    public  void sendJsonArrayToServer(final JSONArray getJson) {

        mVolleySingleton= VolleySingleton.getInstance();
        mRequestQueue=mVolleySingleton.getRequestQueue();

        JsonArrayRequest jsonrequest=new JsonArrayRequest(Request.Method.POST,server_url,getJson
                ,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("SenderReciver", " : response ");
                Log.d("SenderReciver", response.toString() + " : response ");
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("SenderReciver","error    "+error.toString());
            }
        });
        mRequestQueue.add(jsonrequest);
    }


    public void sendSMS(){
        mVolleySingleton= VolleySingleton.getInstance();
        mRequestQueue=mVolleySingleton.getRequestQueue();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, server_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("ServerResponse", response.toString() + " : response ");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //Now you can manipulate/store your JSON

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ServerError", "error    " + error.toString());
            }
        });
        mRequestQueue.add(stringRequest);
    }


     /*
    //To get JSON Object of No of guest, Date and Time.
    public JSONObject getJSONDateTimeObject(String[] args) {
        JSONObject ob = new JSONObject();
        try {
            ob.put("GUESTNUMBER", args[0]);
            ob.put("VISITDATE", args[1]);
            ob.put("VISITTIME", args[2]);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ob;
    }
    */

    /*
    *Method for sending data on server as json array.
    *May be useful at later stage.


    public void encodeToJson(List<AlaCarteCardInfo> cuisines) {

        getJson=new JSONArray();
        for(int i=0;i<cuisines.size();i++){

            getJson.put(cuisines.get(i).getJSONObject());

        }
        Log.d("ndroidhouse", getJson.toString() );
        //sendToServer(getJson);
    }


    //Send data to server using JSON-Array.

    public  void sendToServer(final JSONArray getJson) {

        mVolleySingleton= VolleySingleton.getInstance();
        mRequestQueue=mVolleySingleton.getRequestQueue();

        JsonArrayRequest jsonrequest=new JsonArrayRequest(Request.Method.POST,server_url,getJson
                ,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("ndroidhouse", " : response ");
                Log.d("ndroidhouse", response.toString() + " : response ");
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("ndroidhouse","error    "+error.toString());
                sendToServer(getJson);
            }
        });
        mRequestQueue.add(jsonrequest);
    }
    */

    /*
    //Method for sending data at server in the form of key-value pair through map.
    public void sendToServer(final String guestNumber, final String visitDate, final String visitTime){
        mVolleySingleton= VolleySingleton.getInstance();
        mRequestQueue=mVolleySingleton.getRequestQueue();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, server_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("ServerResponse", response.toString() + " : response ");
                            JSONObject obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Now you can manipulate/store your JSON

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ServerError", "error    " + error.toString());
            }
        }) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("GUESTNUMBER", guestNumber);
                params.put("VISITDATE", visitDate);
                params.put("VISITTIME", visitTime);
                return params;
            }
        };

    }
    */

}
