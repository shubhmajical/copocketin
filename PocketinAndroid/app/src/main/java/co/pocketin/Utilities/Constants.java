package co.pocketin.Utilities;

/**
 * Created by shubham on 19-Dec-15.
 * Contains all constants
 */
public class Constants {
    public static final int NUMBER_OF_TABS=5;
    public static final String MALE_GUEST_NUMBER = "male_guest_number";
    public static final String FEMALE_GUEST_NUMBER = "female_guest_number";
    public static final String DATE_OF_VISIT = "date_of_request";
    public static final String TIME_OF_VISIT = "time_of_request";
    public static final String REST_ID = "restaurant_id";
    public static final String USER_CONTACT_NUMBER = "user_contact_number";
    public static final String USER_EMAIL_ID = "user_email_id";

    //Google Places AutoComplete
    public static final String API_NOT_CONNECTED = "Google API not connected";
    public static final String SOMETHING_WENT_WRONG = "OOPs!!! Something went wrong...";
    public static String PlacesTag = "Google Places Auto Complete";

    //URL of pocketin terms and conditions page.
    public static final String TERMS_AND_CONDITIONS_URL = "http://pocketin.co/termsandconditions.html";

    //URL of the host of Pocketin App
    public static final String POCKETIN_SERVER_URL = "https://pocketinbackend-pocketin.rhcloud.com/";

    //Restaurant_Deal to fill Deal_Time Table and to send notifications about the restaurants selected for deal
    public static final String RESTAURANT_DEAL_REQUEST_SERVER_URL = POCKETIN_SERVER_URL+"restaurant_deal/get_deal";
    //Rest_Deal to send deal given by the Admin Panel
    public static final String RESTAURANT_DEAL_GIVEN_SERVER_URL = POCKETIN_SERVER_URL+"restaurant_deal/put_deal";
    //Rest_Deal finally picked up by the User to be saved in Deal_Grabbed Table
    public static final String DEAL_GRABBED_BY_USER_URL = POCKETIN_SERVER_URL+"deal_time/put_deal";

    //RestInfo backend page to fill AlaCartecardInfo
    public static final String RestCardInfo_SERVER_URL = POCKETIN_SERVER_URL+"restaurant_info/card_info";

    //User_Info
    public static final String UserInfo_Server_URL = POCKETIN_SERVER_URL+"user_info/insert_user_info";
    //User_Phone
    public static final String UserPhone_Server_URL = POCKETIN_SERVER_URL+"user_info/insert_user_phone";


}
