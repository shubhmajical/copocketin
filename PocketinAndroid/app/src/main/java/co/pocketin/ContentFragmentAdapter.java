package co.pocketin;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import java.util.List;

import co.pocketin.Fragment.EventFragment;
import co.pocketin.Fragment.AlaCarteFragment;
import co.pocketin.Fragment.BlogFragment;
import co.pocketin.Fragment.BuffetFragment;
import co.pocketin.Fragment.PackagesFragment;
import co.pocketin.Utilities.Constants;

/**
 * This Java file select the fragment corresponding to particular tab.
 * i.e. home tab for home fragment.
 */
class ContentFragmentAdapter extends FragmentStatePagerAdapter {
    private Context c;
    List<AlaCarteCardInfo> list;

    public ContentFragmentAdapter(FragmentManager fragmentManager, Context context,
                                  List<AlaCarteCardInfo> list) {
        super(fragmentManager);
        c = context;
        this.list=list;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return Constants.NUMBER_OF_TABS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int fragment_index) {

        switch (fragment_index) {
            case 0:
                return AlaCarteFragment.newInstance(list);
            case 1:
                return BlogFragment.newInstance();
            case 2:
                return PackagesFragment.newInstance();
            case 3:
                return EventFragment.newInstance();
            case 4:
                return BuffetFragment.newInstance();
            default:
                return null;
        }
    }


}