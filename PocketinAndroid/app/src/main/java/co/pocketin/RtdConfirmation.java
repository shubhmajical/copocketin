package co.pocketin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import co.pocketin.Utilities.Constants;

public class RtdConfirmation extends AppCompatActivity implements View.OnClickListener {

    String restaurantDetails;
    JSONObject jsonObject;
    TextView bookingID, restaurantName, restaurantLocation, restaurantCity, visitorDetails, deal;
    ImageLoader imageLoader ;
    protected NetworkImageView restaurantImage;
    String user_name, bookingIDStr, restNameStr, restLocStr, dealStr, guestNumberStr, visitDateStr, visitTimeStr, userContactNumberStr;
    SharedPreferences pref;
    Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.pocketin.R.layout.activity_rtd_confirmation);

        Intent intent = getIntent();
        restaurantDetails = intent.getStringExtra("CARD_DETAIL");

        Log.d("Rahul", restaurantDetails);

        bookingID = (TextView) findViewById(co.pocketin.R.id.booking_id);
        restaurantName = (TextView) findViewById(co.pocketin.R.id.restaurant_name);
        restaurantImage = (NetworkImageView) findViewById(co.pocketin.R.id.restaurant_network_image);
        restaurantLocation = (TextView) findViewById(co.pocketin.R.id.restaurant_location);
        restaurantCity = (TextView) findViewById(co.pocketin.R.id.restaurant_city);
        visitorDetails = (TextView) findViewById(co.pocketin.R.id.visitor_details);
        deal = (TextView) findViewById(co.pocketin.R.id.deal);

        imageLoader = VolleySingleton.getInstance().getImageLoader();

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        user_name = pref.getString("user_name", null);
        userContactNumberStr = pref.getString("user_contact_number", null);

        cancel = (Button) findViewById(co.pocketin.R.id.cancel);
        cancel.setOnClickListener(this);

        try {


            jsonObject = new JSONObject(restaurantDetails);

            /*
            //Updating JsonObject to be sent to server to fill Deal_Grabbed Table
                jsonObject.put("DealProposedID",jsonObject.get("Deal_Proposed"));
                jsonObject.put("UserEmail",pref.getString("user_email", null));
                jsonObject.put("DealTimeID",pref.getString("DealTimeID",null));

            //ToDo put the confirmation URL here:
            new SenderReceiver(Constants.DEAL_GRABBED_BY_USER_URL).sendUserDataToServer(jsonObject);
            Log.d("ConfirmationDetails", jsonObject.toString() + "");*/

            guestNumberStr = Integer.parseInt(jsonObject.getString("MALE_GUEST_NUMBER")) + Integer.parseInt(jsonObject.getString("FEMALE_GUEST_NUMBER")) + "";
            visitDateStr = jsonObject.getString("VISIT_DATE");
            visitTimeStr = jsonObject.getString("VISIT_TIME");
            bookingID.setText("Booking ID: " + generateBookingID());
            restaurantName.setText(jsonObject.getString("RESTAURANT_NAME"));
            restaurantImage.setImageUrl(jsonObject.getString("RESTAURANT_IMAGE"),imageLoader);
            restaurantLocation.setText(jsonObject.getString("LOCATION"));
            restaurantCity.setText("New Delhi");
            deal.setText("Deal - " + jsonObject.getString("FETCHED_DEAL"));
            visitorDetails.setText(guestNumberStr + " pax\t\t\t" + visitDateStr + "\t\t\t" + visitTimeStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toolbar toolbar = (Toolbar) findViewById(co.pocketin.R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        bookingIDStr = (String) bookingID.getText();
        restNameStr = (String) restaurantName.getText();
        restLocStr = (String) restaurantLocation.getText();
        dealStr = (String) deal.getText();
        String routeSmsURL = "http://sms6.routesms.com:8080/bulksms/bulksms?username=pocketin&password=sd34rfgg&type=0&dlr=0&destination=" + userContactNumberStr + "&source=PCKTIN&message=Dear%20" + user_name.replace(" ", "%20") + ",%0A%0A" + bookingIDStr.replace(" ", "%20") + "%0AYour%20table%20has%20been%20reserved%20at%20" + restNameStr.replace(" ", "%20") + ",%20" + restLocStr.replace(" ", "%20") + "%20on%20" + visitDateStr + "%20at%20" + visitTimeStr.replace(" ", "%20") + "%20for%20" + guestNumberStr + "%20pax%20with%20" + dealStr.replace(" ", "%20") + "%0A%0ACall-%20+91%209582130421%20for%20any%20queries.%0A%0ARegards,%0ATeam%20Pocketin";
        new SenderReceiver(routeSmsURL).sendSMS();
        routeSmsURL = "http://sms6.routesms.com:8080/bulksms/bulksms?username=pocketin&password=sd34rfgg&type=0&dlr=0&destination=9910558251&source=PCKTIN&message=Dear%20" + user_name.replace(" ", "%20") + ",%0A%0A" + bookingIDStr.replace(" ", "%20") + "%0AYour%20table%20has%20been%20reserved%20at%20" + restNameStr.replace(" ", "%20") + ",%20" + restLocStr.replace(" ", "%20") + "%20on%20" + visitDateStr + "%20at%20" + visitTimeStr.replace(" ", "%20") + "%20for%20" + guestNumberStr + "%20pax%20with%20" + dealStr.replace(" ", "%20") + "%0A%0ACall-%20+91%209582130421%20for%20any%20queries.%0A%0ARegards,%0ATeam%20Pocketin";
        new SenderReceiver(routeSmsURL).sendSMS();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Arrays.fill(AlaCarteCardAdapter.isSelected, Boolean.FALSE);
        AlaCarteCardAdapter.CardViewHolder.selected_restaurant_list.clear();
        RtdCardAdapter.deals_updation = false;
        Intent intent = new Intent(this, SplashActivity.class);
        startActivity(intent);
    }

    private String generateBookingID() {
        //generate a 3 digit integer 100 <1000
        int randomPIN1 = (int) (Math.random() * 900) + 100;
        int randomPIN2 = (int) (Math.random() * 900) + 100;
        return "POC" + String.valueOf(randomPIN1) + String.valueOf(randomPIN2);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == co.pocketin.R.id.cancel) {
            new PhoneCallMethod().cancelDeal(this, user_name, (String) restaurantName.getText(), visitDateStr, visitTimeStr);
        }
    }
}
